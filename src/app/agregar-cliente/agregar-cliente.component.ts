import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import 'firebase/storage';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.scss']
})
export class AgregarClienteComponent implements OnInit {

  formularioCliente: FormGroup;
  porcentajeSubida; number = 0;
  urlImagen: string = '';
  esEditable: boolean = false;
  id: string;

  constructor(private fb: FormBuilder,
    private storage: AngularFireStorage,
    private afs: AngularFirestore,
    private activatedRoute: ActivatedRoute,
    private mensajesService: MensajesService) { }

  ngOnInit() {
    this.formularioCliente = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      correo: ['', Validators.compose([
        Validators.required, Validators.email
      ])],
      dni: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      telefono: [''],
      imgUrl: [''],
    });
    this.id = this.activatedRoute.snapshot.params.clienteID;
    if (this.id !== undefined) {
      this.esEditable = true;

      console.log('ES EDITABLE  ---> LIMPIA VALIDATORS');
      this.formularioCliente.controls['imgUrl'].clearValidators();

      this.afs.doc<any>(`clientes/${this.id}`).valueChanges().subscribe(cliente => {
        console.log('ONINIT', cliente);
        this.formularioCliente.setValue({
          nombre: cliente.nombre,
          apellido: cliente.apellido,
          correo: cliente.correo,
          dni: cliente.dni,
          fechaNacimiento: new Date(cliente.fechaNacimiento.seconds * 1000).toISOString().substr(0, 10),
          telefono: cliente.telefono,
          imgUrl: ''
        });
        this.urlImagen = cliente.imgUrl;
        console.log('URLIMAGEN RECUPERADA', this.urlImagen);
      });
    } else {
      console.log('NO ES EDITABLE ---> AGREGA VALIDATORS');
      this.formularioCliente.controls['imgUrl'].setValidators([Validators.required]);
    }
  }

  agregar() {
    this.formularioCliente.value.imgUrl = this.urlImagen;
    this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento);
    this.afs.collection('clientes').add(this.formularioCliente.value)
    .then(termino => {
      this.mensajesService.mensajeCorrecto('Agregado', 'Se agregó correctamente', 'Aceptar');
    })
    .catch(error => {
      this.mensajesService.mensajeError('Error', 'Ocurrió un error al agregar el cliente', 'Aceptar');
    });
  }

  editar() {
    this.formularioCliente.value.imgUrl = this.urlImagen;
    console.log('IMAGEN A SUBIR', this.urlImagen);
    this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento);
    this.afs.doc<any>(`clientes/${this.id}`).update(this.formularioCliente.value)
    .then(termino => {
      this.mensajesService.mensajeCorrecto('Editado', 'Se editó correctamente', 'Aceptar');
    })
    .catch(error => {
      this.mensajesService.mensajeError('Error', 'Ocurrió un error al actualizar el cliente', 'Aceptar');
    });
  }

  subirImagen(evento) {
    if (evento.target.files.length > 0) {
      const nombre = new Date().getTime().toString();
      const file = evento.target.files[0];
      const extension = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const filePath = `clientes/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);
      task.then(objeto => {
        console.log('Imagen subida');
        ref.getDownloadURL().subscribe(url => {
          this.urlImagen = url;
          console.log('URLIMAGEN CAMBIADA', this.urlImagen);
        });
      });
      task.percentageChanges().subscribe(porcentaje => {
        console.log(porcentaje);
        this.porcentajeSubida = parseInt(porcentaje.toString());
      });
    }
  }
}
