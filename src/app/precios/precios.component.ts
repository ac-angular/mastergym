import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MensajesService } from '../services/mensajes.service';
import { Precio } from '../models/precio';

@Component({
  selector: 'app-precios',
  templateUrl: './precios.component.html',
  styleUrls: ['./precios.component.scss']
})
export class PreciosComponent implements OnInit {

  formularioPrecio: FormGroup;
  precios: Precio[] = new Array<Precio>();
  esEditar: boolean = false;
  id: string;

  constructor(private fb: FormBuilder, private afs: AngularFirestore, private mensajesService: MensajesService) { }

  ngOnInit() {
    this.formularioPrecio = this.fb.group({
      nombre: ['', Validators.required],
      costo: ['', Validators.required],
      duracion: ['', Validators.required],
      tipoDuracion: ['', Validators.required]
    });
    this.mostrarPrecios();
  }

  mostrarPrecios() {
    this.precios.length = 0;
    this.afs.collection<Precio>('precios').get().subscribe(resultado => {
      resultado.docs.forEach(dato => {
        const precio = dato.data() as Precio;
        precio.id = dato.id;
        precio.ref = dato.ref;
        this.precios.push(precio);
      });
    });
  }

  agregar() {
    console.log(this.formularioPrecio.value);
    this.afs.collection<Precio>('precios').add(this.formularioPrecio.value)
    .then(() => {
      this.mensajesService.mensajeCorrecto('Agregado', 'Agregado correctamente', 'Aceptar');
      this.formularioPrecio.reset();
      this.mostrarPrecios();
    })
    .catch(() => {
      this.mensajesService.mensajeError('Error', 'Ocurrió un error', 'Aceptar');
    });

  }

  editarPrecio(precio: Precio) {
    this.esEditar = true;
    this.formularioPrecio.setValue({
      nombre: precio.nombre,
      costo: precio.costo,
      duracion: precio.duracion,
      tipoDuracion: precio.tipoDuracion
    });
    this.id = precio.id;

  }

  editar() {
    this.afs.doc(`precios/${this.id}`).update(this.formularioPrecio.value)
    .then(() => {
      this.mensajesService.mensajeCorrecto('Editado', 'Se editó correctamente', 'Aceptar');
      this.formularioPrecio.reset();
      this.mostrarPrecios();
      this.esEditar = false;
    })
    .catch(error => {
      this.mensajesService.mensajeError('Error', 'Ocurrió un error', 'Aceptar');
    });
  }

}
