import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.scss']
})
export class ListadoClientesComponent implements OnInit {

  clientes: any[] = new Array<any>();
  constructor(private firestore: AngularFirestore) { }

  ngOnInit() {
    this.clientes.length = 0;
    this.firestore.collection('clientes').get().subscribe(resultado => {
      resultado.docs.forEach(item => {
        const cliente = item.data();
        cliente.id = item.id;
        cliente.rf = item.ref;
        this.clientes.push(cliente);
      });
    });
  }

}
