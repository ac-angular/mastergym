import { Component, OnInit } from '@angular/core';
import { Inscripcion } from '../models/inscripcion';
import { Cliente } from '../models/cliente';
import { AngularFirestore } from '@angular/fire/firestore';
import { Precio } from '../models/precio';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  styleUrls: ['./inscripcion.component.scss']
})
export class InscripcionComponent implements OnInit {

  inscripcion: Inscripcion = new Inscripcion();
  clienteSeleccionado: Cliente = new Cliente();
  precioSeleccionado: Precio = new Precio();
  precios: Precio[] = new Array<Precio>();
  idPrecio: string = 'null';

  constructor(private afs: AngularFirestore, private mensajesService: MensajesService) { }

  ngOnInit() {
    this.afs.collection('precios').get().subscribe(resultado => {
      resultado.docs.forEach(item => {
        const precio = item.data() as Precio;
        precio.id = item.id;
        precio.ref = item.ref;
        this.precios.push(precio);
      });
    });
  }

  asignarCliente(cliente: Cliente) {
    this.inscripcion.cliente = cliente.ref;
    this.clienteSeleccionado = cliente;
    console.log(cliente);
  }

  eliminarCliente() {
    this.clienteSeleccionado = new Cliente();
    this.inscripcion.cliente = undefined;
  }

  guardar() {
    if (this.inscripcion.validar().esValido) {
      const inscripcion = {
        cliente: this.inscripcion.cliente,
        precios: this.inscripcion.precios,
        fecha: this.inscripcion.fecha,
        fechaFinal: this.inscripcion.fechaFinal,
        impuesto: this.inscripcion.impuesto,
        suTotal: this.inscripcion.subTotal,
        total: this.inscripcion.total
      }
      console.log('LO QUE DESEAMOS GUARDAR', this.inscripcion);
      this.afs.collection('inscripciones').add(inscripcion)
      .then(resultado => {
        this.inscripcion = new Inscripcion();
        this.clienteSeleccionado = new Cliente();
        this.precioSeleccionado = new Precio();
        this.idPrecio = 'null';
        this.mensajesService.mensajeCorrecto('Guardado', 'Se guardo correctamente', 'Aceptar');
      });
    } else {
      this.mensajesService.mensajeAdvertencia('Advertencia', this.inscripcion.validar().mensaje, 'Aceptar');
    }
  }

  seleccionarPrecio(id: string) {
    if (id !== 'null') {
      this.precioSeleccionado = this.precios.find(x => x.id === id);
      this.inscripcion.precios = this.precioSeleccionado.ref;
      this.inscripcion.fecha = new Date();
      const anioActual: number = this.inscripcion.fecha.getFullYear();
      const mesActual: number = this.inscripcion.fecha.getMonth();
      const diaActual: number = this.inscripcion.fecha.getDate();
      let fechaFinal: Date = new Date();
      let dias: number;

      switch (this.precioSeleccionado.tipoDuracion) {
        case 1:
          dias = this.precioSeleccionado.duracion;
          fechaFinal = new Date(anioActual, mesActual, diaActual + dias);
          break;
        case 2:
          dias = this.precioSeleccionado.duracion * 7;
          fechaFinal = new Date(anioActual, mesActual, diaActual + dias);
          break;
        case 3:
          dias = this.precioSeleccionado.duracion * 15;
          fechaFinal = new Date(anioActual, mesActual, diaActual + dias);
          break;
        case 4:
          const meses: number = this.precioSeleccionado.duracion;
          fechaFinal = new Date(anioActual, mesActual + meses, diaActual);
          break;
        case 5:
          const anios: number = this.precioSeleccionado.duracion;
          fechaFinal = new Date(anioActual + anios, mesActual, diaActual);
          break;
      }
      this.inscripcion.fechaFinal = fechaFinal;

      this.inscripcion.subTotal = this.precioSeleccionado.costo;
      this.inscripcion.impuesto = this.inscripcion.subTotal * 0.15;
      this.inscripcion.total = this.inscripcion.subTotal + this.inscripcion.impuesto;
    } else {
      this.precioSeleccionado = new Precio();
      this.inscripcion.precios = null;
      this.inscripcion.subTotal = 0;
      this.inscripcion.impuesto = 0;
      this.inscripcion.total = 0;
      this.inscripcion.fecha = null;
      this.inscripcion.fechaFinal = null;
    }
  }
}
