import { Component, OnInit } from '@angular/core';
import { Inscripcion } from '../models/inscripcion';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-listado-inscripciones',
  templateUrl: './listado-inscripciones.component.html',
  styleUrls: ['./listado-inscripciones.component.scss']
})
export class ListadoInscripcionesComponent implements OnInit {

  inscripciones: any[] = new Array<any>();
  constructor(private afs: AngularFirestore) { }

  ngOnInit() {
    this.inscripciones.length = 0;
    this.afs.collection('inscripciones').get().subscribe(respuesta => {
      respuesta.forEach(inscripcion => {
        const inscripcionObtenida = inscripcion.data();
        inscripcionObtenida.id = inscripcion.id;

        this.afs.doc(inscripcion.data().cliente.path).get().subscribe(cliente => {
          inscripcionObtenida.clienteObtenido = cliente.data();
          inscripcionObtenida.fecha = new Date(inscripcionObtenida.fecha.seconds * 1000);
          inscripcionObtenida.fechaFinal = new Date(inscripcionObtenida.fechaFinal.seconds * 1000);
          this.inscripciones.push(inscripcionObtenida);
          console.log(inscripcionObtenida);

        });
      });
    });
  }

}
